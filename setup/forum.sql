-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 10, 2020 at 10:14 PM
-- Server version: 10.3.22-MariaDB-log-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE `forum` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `category` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `icon` varchar(100) NOT NULL,
  `type` int(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1 = everyone | 2 = staff | 3 = staff post + everyone see'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forum`
--

INSERT INTO `forum` (`id`, `name`, `category`, `description`, `icon`, `type`) VALUES
(1, 'Latest news', 1, 'Latest news for the community.', '/forums/forum_007.png', 3),
(2, 'Rules & Guidelines', 1, 'Forums Guidelines and Rules.', '/forums/forum_005.png', 3),
(13, 'Guild Recruitment & Search', 3, 'Recruit or find a guild here! Discussion are welcome.', '/forums/forum_013.png', 1),
(3, 'Events', 1, 'Information about events and applications to participate.', '/forums/forum_009.png', 3),
(4, 'Frequently Asked Questions', 1, 'Small knowledge base about questions that are asked the most and their common answers.', '/forums/forum_016.png', 3),
(5, 'Suggestions & Changes', 2, 'Give us your thoughts on anything server and website related.', '/forums/forum_002.png', 1),
(6, 'Support & Q/A', 2, 'A place for players to help players', '/forums/forum_020.png', 1),
(7, 'Ask the Staff', 2, 'The place to contact the Community Managers about Events and In-Game questions.', '/forums/forum_018.png', 1),
(8, 'Guides & Tutorials', 2, 'Create guides to help the community!', '/forums/forum_011.png', 1),
(12, 'Introduce Yourself', 3, 'New to SERVERNAME? Tell us a bit about yourself.', '/forums/forum_010.png', 1),
(10, 'General Discussion', 3, 'General Discussion Discussion about World of Warcraft. This section is not meant to ask support questions.', '/forums/forum_001.png', 1),
(11, 'Realm Discussion', 3, 'General discussion for realm, REALMNAME.', '/forums/forum_012.png', 1),
(14, 'Off Topic', 3, 'Talk about anything non World of Warcraft related.', '/forums/forum_017.png', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forum`
--
ALTER TABLE `forum`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forum`
--
ALTER TABLE `forum`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
